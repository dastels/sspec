# sSpec Manual

_Copyright 2006-2014 David R. Astels_


##￼What Is sSpec?

sSpec is a framework for writing executable specifications for Squeak Smalltalk programs. Expectations in sSpec take the form of chains of message sends. For example:

      stack top should not equal: 3

The target object here is the result of `stack top`, the message `should` is sent to the target, which returns a `ShouldHelper` instance. This is then sent the `not` message, returning a `ShouldNegator`, which is sent the `equal:` message with the `3` as an argument. If this answers `true` the expectation is met, otherwise it fails.

##￼Core API

sSpec defines a method `should` on `Object` so it is available on every object in the system. This `should` method is your entry to the magic of sSpec.

Almost all expectation forms have a corresponding negated form. It is listed when it is supported and, unless otherwise stated, is met when ever the non-negated form would be violated.

### General 

**Arbitrary Block**

      target should satisfy: [:target | ...]
      target should not satisfy: [:target | ...]
      
The supplied block is evaluated, passing `target` as the sole argument. If the block evaluates to `false`, the expectation fails.

      target should satisfy: [:arg | arg > 0]

**Equality**

      target should equal: value
      target should not equal: value

The target object is compared to value using `=`. If the result is `false`, the expectation fails.

**Floating Point Comparison**

      target should be within: tolerance of: value
      target should not be within: tolerance of: value

The target object is compared to `value`. If they differ by more than `tolerance`, the expectation fails. In the negated case, the expectation fails if they differ by less than `tolerance`.

      target should be within: 0.05 of: 27.35
      
**Identity**

      target should be: value
      target should not be: value
      
The target object is compared to value using `==`. If the result is `false`, the expectation fails.

**Arbitrary Predicate**

      target should predicate
      target should be predicate
      target should not predicate
      target should not be predicate

The message `predicate` can be any selector understood by `target`. It will be sent to `target` and if the result is `false`, the expectation fails. Note, that it can be any selector, even though only a unary selector is shown here.
If the given selector is not understood by `target`, sSpec tries prepending (and capitalizing) _is_ and _has_ to find a selector that `target` understands. For example: `container should be empty` results in `container isEmpty`.

**Class/Type Direct Instance**

      target should be an instance of: class
      target should not be an instance of: class

An the expectation fails if `target` is not or is, respectively, an instance of `class`. As expected this correlates to `target isMemberOf: class`.

**Ancestor Class**

      target should be a kind of: class
      target should not be a kind of: class

As above, but uses `target isKindOf: class` checking whether class is the direct class of `target`, or an ancestor of `target`'s direct class.

**Type**

target should respond to: selector
target should not respond to: selector

Uses `target respondsTo: symbol` to check whether `target` understands `selector`. 

### Blocks

**Raising**

      aBlock should raise: exception
      aBlock should not raise: exception

Checks whether evaluating `aBlock` causes the named `exception` to be raised. The latter is actually one of two cases: some other exception is raised, or no exception is raised. For example:

      [3 / 0] should raise: ZeroDivide

There is a more general form as well.

      aBlock should raise
      aBlock should not raise

￼These forms don't care what exception is raised. All they are concerned with is that some exception was, or that no exception was.

### Collections 

**Containment**

      target should include: object
      target should not include: object

This is simply a specific case of the arbitrary predicate form. It uses `target includes: object` and fails if that returns `false`.

The remaining collection forms are a little more involved. They rely on two things: 1) that `target` responds to the message `things` by answering an object that 2) responds to either `length` or `size`. `length` is used if is understood, otherwise `size` is attempted.

**Exact Size**

      target should have: number in: #things

The `things` of `target` has a length/size of exactly `number`. 

**Lower Bound**

      target should have at least: number in: #things

The `things` of `target` has a length/size of no less than `number`. 

**Upper Bound**

      target should have at most: number in: #things

The `things` of `target` has a length/size of no more than `number`.

## Mock API

sSpec contains a full featured _Mock Objects_ framework.

**Creating a mock**

      mock := Mock named: name
      
This creates a new mock with the given `name` (a string) and registers it. When the specification finishes running, all registered mocks are automatically verified.

      mock := Mock named: name withOptions: options

As above, but allows you to specific options to tweak the mock's behavior. The options argument is an array. Currently the only supported option is `#nullObject`. Setting this instructs the mock to ignore (quietly consume) any messages it hasn't been told to expect. I.e.:

      mock := Mock named: "blah" withOptions: #(nullObject)

## Expecting Messages

      mock shouldReceive: #selector

This tells the mock to expect `selector` to be sent to it. 

## Arbitrary Message Handling

You can supply a block to a message expectation. When the message is received by the mock, the block is evaluated, and passed any arguments. The result of evaluating the block is returned by the message. For example:

      (mock shouldReceive: #randomCall)
            andDo: [:a | a should be true]

This allows arbitrary argument validation and result computation. It's handy to be able to do this, but I advise against it. Mocks should not be functional. They should be completely declarative. That said, it's sometimes useful to give them some minimal behavior.

## Expecting Arguments

      (mock shouldReceive: #msg) with: arg

for example:

      (mock should receive: #msg) with: 1

There are forms supporting up to 4 arguments, as well as a form supporting an array of arguments:

      (mock shouldReceive: #msg) with: arg1 with: arg2
      (mock shouldReceive: #msg) with: arg1 with: arg2 with: arg3
      (mock shouldReceive: #msg) with: arg1 with: arg2 with: arg3 with: arg4
      (mock shouldReceive: #msg) withAll: args
 
￼There are two special forms as well:

      (mock shouldReceive: #msg) withNoArgs

The message `msg` is expected to be passed no arguments. 

      (mock shouldReceive: #msg) withAnyArgs
      
Any arguments (and any number of arguments) are to be accepted. This includes cases where no arguments are provided. _This is the default when no `with` clause is specified._ Even so, sometimes you want to be explicit about it.

## Argument Constraints

Constraints can be placed on individual arguments which are looser than value equivalence (as above).

**#anything**

Accepts any value of any type for this argument, e.g.:

      (mock shouldReceive: #msg:with:) with: 1 with: #anything
      
This expectation will be met if the mock is passed the message `msg:with:` with the first argument being 1 and the second ignored.

**#numeric**

Accepts any numeric value for this argument, e.g.:

      (mock shouldReceive: #msg:with:) with: 1 with: #numeric

This expectation will be met if the mock is passed the message `msg:with:` with the first argument being 1 and the second having any numeric value.

**#boolean**

Accepts any boolean value for this argument, e.g.:

      (mock shouldReceive: #msg) with: 1 with: #boolean

This expectation will be met if the mock is passed the message `msg:with:` with the first argument being 1 and the second being a boolean.

**#string**

Accepts any string value for this argument, e.g.:

      (mock shouldReceive: #msg) with: 1 with: #string
 
This expectation will be met if the mock is passed the message `msg:with:` with the first argument being 1 and the second being a string.

**duck typing**

Accepts any object that responds to all of the prescribed selectors (1 or more).

      DuckTypeArgConstraint with: #size

There are forms with up to 4 with: arguments as well as withAll: form that takes a collection of selectors

      (mock shouldReceive: #randomCall:)
            anyNumberOfTimes;
            with: (DuckTypeArgConstraint with: #size with: #includes:).
      (mock shouldReceive: #randomCall:)
                  with: (DuckTypeArgConstraint withAll: #(size do: collect: select: detect:))

## Receive Counts 

You can specify how often to expect a particular message to be received. The message expectation will be unsatisfied if the number of times the message is received doesn't match the expectation. The default is `once`.

**never**

￼The expectation fails if the message is ever received.

      (mock shouldReceive: #msg) never

**anyNumberOfTimes**

The message can be received 0 or more times. This is the most general case.

      (mock shouldReceive: #msg) anyNumberOfTimes

**once**

The expectation fails if the message is never received, or it is received more than once. This is the default expectation.

      (mock shouldReceive: #msg) once

**twice**

The expectation fails if the message is received anything but two times.

      (mock shouldReceive: #msg) twice

**thrice**

The expectation fails if the message is received anything but three times.  This is a bit odd, but I include because Dave Chelimsky got such a kick out of it when I had it in rSpec's early mocking framework.

      (mock shouldReceive: #msg) thrice

**exactly:**

The expectation fails if the message is received anything but `n` times. 

      (mock shouldReceive: #msg) exactly: n times

**atLeastOnce**

The expectation fails if the message is never received. Any number of additional times is acceptable.

      (mock shouldReceive: #msg) atLeastOnce

**atLeastTwice**

The expectation fails if the message is never received or is received only once. Any number of additional times is acceptable.

      (mock shouldReceive: #msg) atLeastTwice

**atLeast:**

The expectation fails if the message is received fewer than `n` times. Any number of additional times is acceptable.

      (mock shouldReceive: #msg) atLeast: n times

**atMost:**

The expectation fails if the message is received more than n times. Any number of times less than or equal to `n` is acceptable.

      (mock shouldReceive: #msg) atMost: n times

## Return Values 

**Single return value**

Each time the expected message is received, `value` will be returned as the result.

￼      (mock shouldReceive: #msg) andReturn: value

**Consecutive return values**

When the expected message is received, `valuei` will be returned as the result for the `ith` reception of the message. After the message has been received `n` times, `valuen` is returned for all subsequent receives. Note that this is unrelated to the receive count as specified above. 

      (mock shouldReceive: #msg) andReturnConsecutively: #(value1 value2 ... valuen)
      
**Computed return value**

When the expected message is received, the result of evaluating the supplied block will be returned as the result. The block is passed any arguments passed as arguments of the message.

      (mock shouldReceive: #msg) andReturn: [...]

This capability can be used to compute return values based on the arguments. For example:

      (mock shouldReceive: #msg:with:)
            once;
            with: #(numeric numeric);
            andReturn: [:a :b| a + b]

## Raising and Throwing
These instruct the mock to raise an exception or throw a symbol, respectively, in response to receiving a message instead of returning a value.

      (mock shouldReceive: #msg) andRaise: exception

## Ordering

There are times when you want to specify the order of messages sent to a mock. It shouldn't be the case very often, but it can be handy at times. The functionality of this feature is intentionally quite limited.

Labeling expectations as being ordered is done with the `ordered` call:

      (mock shouldReceive: #flip) once; ordered.
      (mock shouldReceive: #flop) once; ordered.

If the send of flop is seen before flip the specification will fail. Of course, chains of ordered expectations can be set up:

      (mock shouldReceive: #one) ordered.
      (mock shouldReceive: #two) ordered.
      (mock shouldReceive: #three) ordered.

The expected order is the order in which the expectations are declared.

Order-independant expectations can be set anywhere in the expectation sequence, in any order. Only the order of expectations tagged with the `ordered` call is significant. Likewise, calls to order-independant methods can be made in any order, even interspersed with calls to order-dependant methods. For example:

      mock shouldReceive: #zero.
      (mock shouldReceive: #one) ordered.
      (mock shouldReceive: #two) ordered.
      mock shouldReceive: #onePointFive.
      mock proxy
            one;
            onePointFive;
            zero;
            two

## Using a Mock

Sending `proxy` to a mock answers a proxy object that fronts for the mock in the system under test. It can be passed into the system and used just as a _real_ object would be. Interactions with it are trapped and checked against the expectation that have been set.

      myObj := mock proxy
      
# Executing Expectations

There is only one specification runner: `TextSpecRunner`.

There are two choices to make when creating a `TextSpecRunner` (actually the cross product of 2 ways):

1. the format of the output, terse or verbose, and
2. where the output goes, `Transcript` or a supplied stream.

Output from the spec run can be in one of two forms. **Terse** provides very basic output, for example:

      X.

Where a `.` indicates a specification that is met, and an `X` indicates a failing specification. Running in **verbose** mode provide a list of contexts and specifications as they are being executed, noting which failed:

      sample context with failing spec
            should fail (FAILED 1)
            should pass

In both cases, this output is followed by a summary of how many specs were run, how many failed, and how long the run took:

      2 specs, 1 failures
      0.208 seconds

Typically, a terse run will go much faster. For example, the above run in terse mode reported:

      2 specs, 1 failures
      0.1 seconds

Finally, as well in both cases, any failure will result in a summary of the failure, numbered in order of appearance. These numbers are noted in the verbose output with the failure indication, e.,g.:

      1) ExpectationNotMetError in 'sample context with failing spec should fail'
      5 should be even
      SampleContextWithFailingSpec>>shouldFail

There are four pieces of useful information here:

1. The exception that is responsible for the failure, e.g. `ExpectationNotMetError`
2. The context & spec, identified in a prosy way. This is a good reason for carefully choosing context & spec names... they should read well together and make sense. E.g. `sample context with failing spec should fail`
3. A message specific to the exact failure, e.g. `5 should be even`
4. The class/method of the failing specification, e.g. `SampleContextWithFailingSpec>>shouldFail`