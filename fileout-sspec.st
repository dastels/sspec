"File out SSpec stuff"

(SystemOrganizer default categoriesMatching: 'SSpec*') do: [:c | 
	SystemOrganizer default fileOutCategory: c].

SystemOrganizer default categories do: [:cat | 
	(SystemOrganizer default listAtCategoryNamed: cat) do: [:classname |
		((Smalltalk at: classname) methodsInCategory: '*sspec-support') ifNotEmptyDo: [:methods |
			(Smalltalk at: classname) fileOutCategory: '*sspec-support' ]]]
